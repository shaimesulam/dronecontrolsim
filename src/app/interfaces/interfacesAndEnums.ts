export interface IDroneAndHeight {
  droneID: number;
  assignedHeight: number;
}

export interface IControllerEmition {
  tick: number;
  clearToTakeOff: Array<IDroneAndHeight>;
}
export enum EStatus {
  'en Route' = 1,
  'waiting for passenger',
  'waiting for clearance'
}

import {EStatus, IControllerEmition, IDroneAndHeight} from '../interfaces/interfacesAndEnums';
import {ControllerService} from '../services/controller.service';

export class Drone {
  id: number; // serial id of drone
  state: EStatus = EStatus['waiting for passenger'];
  // minutes till end of state (till end of route or till end of wait for passenger and requesting takeoff permission)
  internalCounter: number;
  assignedHeight: number; // height assigned for this drone -1 for no assigned height

  constructor(id: number, private droneController: ControllerService) {
    this.id = id;
    this.internalCounter = this.randomizeWaitingTime();
    console.log(`Init: Drone #${this.id} is set to wait ${this.internalCounter} minutes for passenger`);
    this.assignedHeight = -1;

  }

  /**
   * this function is ran on each emission of the controller observable, changing drone object's state as required
   * @param payload - the payload in this emission, including assigned heights to drones allowed to take off.
   */
  subscribeFunction = (payload: IControllerEmition) => {
    switch (this.state) {
      case EStatus['en Route']:
        if (this.internalCounter === 0) {
          this.droneController.releaseHeight(this.assignedHeight);
          this.assignedHeight = -1;
          this.internalCounter = this.randomizeWaitingTime();
          console.log(`Minute ${payload.tick}: Drone #${this.id} landed and is expecting a new passenger in ${this.internalCounter} minutes`);
          this.state = EStatus['waiting for passenger'];
        }
        else {
          this.internalCounter--;
        }
        break;
      case EStatus['waiting for passenger']:
        if (this.internalCounter === 0) {
          this.internalCounter = this.getRandomFlightLength();
          this.state = EStatus['waiting for clearance'];
          this.droneController.requestHeight(this.id);
          console.log(`Minute ${payload.tick}: Drone #${this.id} accepted new passenger for a ${this.internalCounter} minutes ride, and is now waiting for clearance to takeoff`);
        }
        else {
          this.internalCounter--;
        }
        break;
      case EStatus['waiting for clearance']:
        // checking if clearance is given
        const assignedHeight = this.isClearToTakeOff(payload);
        if (assignedHeight) {
            this.state = EStatus['en Route'];
            this.assignedHeight = assignedHeight as number;
            console.log(`Minute ${payload.tick}: Drone #${this.id} is clear to fly at height ${assignedHeight} and is setting for a ${this.internalCounter} minutes ride`);
        }
        break;
    }
  }

  /**
   * @param payload array of drone Ids who got clearance to takeoff with their assigned heights
   * @private
   * returns assigend height to this drone if was cleared to take off in this cycle - or false if it was not
   */
  private isClearToTakeOff(payload: IControllerEmition): (boolean | number) {
    const acceptedHeight: IDroneAndHeight[] = payload.clearToTakeOff.filter(
      (droneAndHeight: IDroneAndHeight) => droneAndHeight.droneID === this.id);
    return acceptedHeight.length > 0 ? acceptedHeight[0].assignedHeight : false;
  }

  /**
   * randomize number of minutes from 1 to 5, equivalent to travel time of 1 to 5 km @ 60kph
   */
  private getRandomFlightLength = (): number => Math.floor(Math.random() * 5 + 1);

  /**
   * randomize number of minutes from 1 to 15 in which drone is waiting for passenger
   */
  private randomizeWaitingTime = (): number =>    Math.floor(Math.random() * 15 + 1);

}

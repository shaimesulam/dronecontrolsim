import { Component } from '@angular/core';
import {ControllerService} from './services/controller.service';
import {Drone} from './classes/drone';
import {EStatus} from './interfaces/interfacesAndEnums';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title = 'Drone Control Simulator';
  drones: Drone[];
  states: Array<string> = Object.keys(EStatus).filter(key => isNaN(+key));


  constructor(private dronesController: ControllerService) {
    this.drones = this.dronesController.getDrones();
  }

  getStateClass(state: number): any {
    return {
      enRoute: state === EStatus['en Route'],
      wfp: state === EStatus['waiting for passenger'],
      wfc: state === EStatus['waiting for clearance']
    };
  }
}

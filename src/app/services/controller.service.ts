import { Injectable } from '@angular/core';
import {AppConfig} from '../appConfig';
import {ConnectableObservable, interval, Subject} from 'rxjs';
import {IControllerEmition, IDroneAndHeight} from '../interfaces/interfacesAndEnums';
import {map, multicast, take} from 'rxjs/operators';
import {Drone} from '../classes/drone';

@Injectable({
  providedIn: 'root'
})
export class ControllerService {

  // private heightsToDronesMap: number[] = new Array<number>(AppConfig.NUMBER_OF_ALLOWED_HEIGHTS);

  // array of heights currently available, sorted in descending order, assuming lower numbers are lower heights
  // (and that flying in lower heights is more efficient, since it takes less ascending / descending time)
  private availableHeights: number[] = new Array<number>();
  private waitingDrones: number[] = new Array<number>();


  private drones: Drone[] = new Array<Drone>();
  title = 'Drone Control Simulator';
  private $Controller: ConnectableObservable<IControllerEmition>;

  constructor() {
    // initialize all heights<=>droneIDs map and the available heights array
    for (let i = AppConfig.NUMBER_OF_ALLOWED_HEIGHTS; i > 0 ; i--) {
      this.availableHeights.push(i);
      // this.heightsToDronesMap[i] = -1;
    }
    this.$Controller = (interval(AppConfig.MINUTE_WORTH_IN_MILLISECONDS)
        .pipe(
          take(AppConfig.NUMBER_OF_MINUTES), // limit to number of minutes
          map(tick => { // get new height assignments and add to ticker
            return { tick, clearToTakeOff: this.getNewHeightsAssignments(tick)} as IControllerEmition;
          }),
          // here comes a .map function to map minute to tick and drone launching
          multicast(() => new Subject<{ tick: number, clearToTakeOff: {}[]  }>(
          ))
        )
    ) as ConnectableObservable<IControllerEmition>;

    // initialize drone objects
    for (let i = 1; i <= AppConfig.NUMBER_OF_DRONES; i++) {
      const d = new Drone(i, this);
      this.$Controller.subscribe(d.subscribeFunction);
      this.drones.push(d);
    }
    // connect to Connected Observable
    this.$Controller.connect();
  }

  /**
   * called by drone to add itself to the takeoff waiting list
   * @param droneID - serial number of drone sending this request
   */
  public requestHeight =  (droneID: number): void =>  {
    this.waitingDrones.push(droneID);
  }

  public releaseHeight = (heightID: number) => {
    this.availableHeights.push(heightID);
    this.availableHeights.sort((a, b) => b - a); // sort availableHIghts in descending order
  }

  public getNewHeightsAssignments = (minute: number): Array<IDroneAndHeight> => {
    // array to return: {droneID and its new assigned height} pairs
    // todo check if const is appropriate here. it appears pushing is ok with const...
    const droneAssignments: Array<IDroneAndHeight> = new Array<IDroneAndHeight>();
    let combo: IDroneAndHeight = this.getDroneAndAvailableHeightCombo();
    while (combo.droneID !== undefined && combo.assignedHeight !== undefined) {
      droneAssignments.push(combo);
      combo = this.getDroneAndAvailableHeightCombo();
    }
    // console.log(`Minute ${minute} getNewHeightsAssignments returned `, droneAssignments);
    return droneAssignments;
  }

  /**
   * Assigning available heights to as many drones waiting for takeoff as possible
   */
  private getDroneAndAvailableHeightCombo = () => {
    const drone = this.waitingDrones.shift(); // give priority to drone pushed earlier
    let availableHeight;
    if (drone !== undefined) {
      availableHeight = this.availableHeights.pop();
    }
    if (availableHeight === undefined && drone !== undefined) {
      this.waitingDrones.unshift(drone);
    }
    return {droneID: drone, assignedHeight: availableHeight} as IDroneAndHeight;
  }

  public getDrones = () => this.drones;
}

# Drone Control Simulator

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.12.

## To install and Run

### Clone this repository:
git clone https://shaimesulam@bitbucket.org/shaimesulam/dronecontrolsim.git

### cd to cloned local dir:
cd dronecontrolsim

### install dependencies:
npm install

### Run app:
ng serve -o

<span style="color:blue">Note: View event logging in the console!</span>

## Changing parameters: (number of drones/heights, times etc)
Edit src/app/appConfig.ts
